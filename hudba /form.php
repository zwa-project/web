<?php
    include("_fileDB.php");
    // mam nyni globalni promenou $data

    if (isset($_POST["pridejPoznamku"])) {
        
        $newNote = array(
            "id" => uniqid(),
            "titulek" => $_POST["titulek"],
            "text" => $_POST["text"]
        );
        storeData($data, $newNote);
        header("Location: vypis.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Presentation</title>
</head>
<body>
    <a href="vypis.php">uložená data</a>
    <form method="post">
    <!-- <form form action="form.php" method="POST" enctype="multipart/form-data"> -->
        <p>
            <label>
                Název prezentace
                <input type="text" name="titulek">
            <label>
        </p>
        <p>
            <label>
                Něco o prezentaci
                <textarea name="text" cols="30" rows="10"></textarea>
            <label>
        </p>
        <input type="file" value="Přidej soubor" name="pridejSoubor">
        <input type="submit" value="Uloz poznamku" name="pridejPoznamku">
    </form>
</body>
</html>