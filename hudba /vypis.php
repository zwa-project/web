<?php
    include("_fileDB.php");

    if(isset($_POST['delete'])) {
        deleteNote($data, $_POST['delete']);
        header("Location: vypis.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Presentation</title>
    <style>
        div {
            margin: 10px 0;
            padding: 20px;
            border: 1px solid #a0a0a0;
            border-radius: 10px;
            width: 50%;
        }
    </style>
</head>
<body>
    <a href="form.php">zpět</a>
    <?php
       foreach($data['notes'] as $note) {
           $titulek = htmlspecialchars($note['titulek']);
           $text = htmlspecialchars($note['text']);
           $id = $note['id'];
           echo "<div>";
           echo "<form method=\"post\"><button name=\"delete\" value=\"$id\">Smaz</button></form>";
           echo "<h2>$titulek</h2>";
           echo "<p>$text</p>";
           echo "</div>";
       } 
    ?>
    </body>
</html>