<?php
    $filename = "db.json";

    $data = file_get_contents($filename);
    if ($data == NULL) {
        include("createDB.php");
    }
    $data = json_decode($data, JSON_OBJECT_AS_ARRAY);
    //var_dump($data);

    function storeData($data, $newNote) {
        array_push($data["notes"], $newNote);
        $encodedData = json_encode($data);
        file_put_contents('db.json', $encodedData);
    }

    function deleteNote($data, $noteId) {
        foreach($data['notes'] as $key => $note ) {
            if ($note['id'] == $noteId) {
                unset($data['notes'][$key]);
                break;

            }
        }        
        $encodedData = json_encode($data);
        file_put_contents('db.json', $encodedData);
    }
?>