<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Presentation</title>

    <link rel="stylesheet" href="login_register.css" media="all">

    <script src="login.js"></script>
</head>
<body>
    <div id="header">
		<div id="logo">
            <h1><a href="http://wa.toad.cz/~blazefi2/semestral/web/">presentation</a></h1>
            <p>&copy; Filip Blažek</p>
		</div>
    </div>
    <div id="container">
        <form method="POST" id="login2">
            <div id="pmail">
                <label for="mail">E-mail:</label>
                <input type="text" id="username" name="username" placeholder="Ve tvaru: something@gmail.com"
                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 
                        title="Vyplňte v požadovaném formátu: something@gmail.com" required>
                <span class="span">Vyplňte pole</span>
            </div>

            <div id="ppassword">
                <label for="password">Heslo:</label>
                <input type="password" id="password" name="password" required>
                <span class="span">Vyplňte pole</span>
            </div>

            <div id="lower">
                <input type="submit" value="Přihlásit se">
            </div>

            <label id="register">Nejsi zaregistrovaný? <a href="http://wa.toad.cz/~blazefi2/semestral/web/register.php">Registruj se!</a></label>       
        </form>
    </div>
        <script>
            init();
        </script>
</body>
</html>


    