function validate(udalost) {
    validateName(udalost);
    validateEmail(udalost);
    validatePassword(udalost);
    // validatePassword2(udalost);
    validateSelect(udalost);
}

function validateName(event) {
    var name = document.querySelector('#fullName');
    var line = document.querySelector('#pname');
    if (name.value.length == 0) {
        event.preventDefault();
        line.className = 'error';
    } else {
        line.className = '';
    }
}

function validateEmail(event) {
    var mail = document.querySelector('#mail');
    var line = document.querySelector('#pmail');
    if (email.value.length == 0) {
        event.preventDefault();
        line.className = 'error';
    } else {
        line.className = '';
     }
    if(mail.value.indexOf('@') == -1) {
        event.preventDefault();
        mail.className = 'error';
    } else {
       mail.className = '';
    }
}

 function validatePassword(event) {
     var password = document.querySelector('#password');
     var line = document.querySelector('#ppassword');
     if (password.value.length < 6) {
         event.preventDefault();
         line.className = 'error';
     } else {
         line.className = '';
     }
 }

function validatePassword2(event) {
    var password = document.querySelector('#password').value;
    var password2 = document.querySelector('#password2').value;
    var line = document.querySelector('#ppassword2');
    if (password2 != password) {
        event.preventDefault();
        line.className = 'error';
    } else {
        line.className = '';
    }
}

function validateSelect(event) {
    var select = document.querySelector('#select');
    var line = document.querySelector('#pselect');
    if (select.value == "0") {
        event.preventDefault();
        line.className = 'error';
    } else {
        line.className = '';
    }
}

function init() {
    var formular = document.querySelector('form#login2');
    formular.addEventListener('submit', validate);

    var name = document.querySelector('#fullName');
    var mail = document.querySelector('#mail');
    var password = document.querySelector('#password');
    // var pass2 = document.querySelector('#password2');
    var select = document.querySelector('#inlineFormCustomSelect')

    name.addEventListener('blur', validateName);
    mail.addEventListener('blur', validateEmail);
    password.addEventListener('blur', validatePassword)
    pass2.addEventListener('blur', validatePassword2);
    select.addEventListener('blur', validateSelect);
}