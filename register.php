<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Presentation</title>

    <link rel="stylesheet" href="login_register.css" media="all">

    <script src="register.js"></script>
</head>
<body>
    <div id="header">
		<div id="logo">
            <h1><a href="http://wa.toad.cz/~blazefi2/semestral/web/">presentation</a></h1>
            <p>&copy; Filip Blažek</p>
		</div>
	</div>
    <div id="container2">
        <form method="POST" id="login2">
            <div id="pname">
                <label for="fullName">Uživatelské jméno:</label>
                <input type="text" id="fullName" name="username" placeholder="Například: loco_10"
                        pattern="[a-zA-Z0-9]+"  
                        title="Bez mezer a interpunkce" required/>
                <span class="span">Vyplňte pole</span>

            <div id="pmail">
                <label for="mail">E-mail:</label>
                <input type="text" id="username" name="username" placeholder="Ve tvaru: something@gmail.com"
                        pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" 
                        title="Vyplňte v požadovaném formátu: something@gmail.com" required>
                <span class="span">Vyplňte pole</span>
            </div>

            <div id="ppassword">
                <label for="password">Heslo:</label>
                <input type="password" id="password" name="password" placeholder="Min. 8 znaků, jedna číslice a jedno malé a velké písmeno"
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"
                        title="Heslo musí obsahovat minimálně 8 znaků, jednu číslici a jedno malé a velké písmeno" required>
                <span class="span">Heslo je příliš krátké</span>
            </div>

            <div id="ppassword2">
                <label for="password2">Heslo znovu:</label>
                <input type="password" id="password2" name="password" placeholder="Zadejte heslo ještě jednou"
                        pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}"
                        title="Potvrďte své heslo" required>
                <span class="span">Hesla se liší</span>
            </div>

            <div id="pselect">
                <label for="customSelect">Pohlaví:</label>
                    <label><input id="select" type="radio" name="sex" value="M"> Muž</label>
                    <label><input id="select" type="radio" name="sex" value="F"> Žena</label>
                <span class="span">Zapomněl si vybrat pohlaví</span>
            </div>

            <div id="lower">
                <input type="submit" value="Registrovat se">
            </div>
            
            <label id="login">Jsi zaregistrovaný? <a href="http://wa.toad.cz/~blazefi2/semestral/web/login.php">Přihlaš se!</a></label>
        </form>
    </div>
        <script>
            init();
        </script>
</body>
</html>




    <!-- <div id="container2">
        <form>
            <label for="username">Celé jméno:</label>
            <input type="text" id="username" name="username">
            <label for="mail">E-mail:</label>
            <input type="text" id="mail" name="mail">
            <label for="password">Heslo:</label>
            <input type="password" id="password" name="password">
            <label>Pohlaví:</label>
            <label><input type="radio" name="sex" value="M"> Muž</label>
            <label><input type="radio" name="sex" value="F"> Žena</label>
            <div id="lower">
                <input type="submit" value="Registrovat se">
            </div>
            <label id="login">Jsi zaregistrovaný? <a href="http://wa.toad.cz/~blazefi2/semestral/web/login.php">Přihlaš se!</a></label>
        </form>
    </div> -->