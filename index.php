<!DOCTYPE html>
<html lang="cs">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Presentation</title>
	<link rel="stylesheet" href="main.css" media="all">
	<link rel="stylesheet" href="print.css" media="print">
</head>
<body>
	<div id="hp">
		<div id="header">
			<div id="logo">
				<h1><a href="#">presentation</a></h1>
			</div>
		</div>
		<div id="menu">
			<ul>
				<li class="current_page_item"><a class="button" href="login.php">Příhlášení</a></li>
				<li class="last"><a class="button" href="register.php">Registrace</a></li>
			</ul>
		</div>
		<div id="banner"><img src="images/screenshot.jpg" width="800" height="473" alt=""></div>



		<div class="container">
			<div class="parent">
				<div class="child">
					<h2> <img src="images/icon.png" alt="Icon"> Příroda</h2>
						<p><img src="images/priroda.jpeg" width="430" height="200" alt="" /></p>
						<p>Máš mít prezentaci a nevíš si rady? Zde nalezneš různé prezentace na téma PŘÍRODA.
							Každou z nich si můžeš stáhnout a použít dle libosti. Můžeš zde hledat i pouhou inspiraci nebo sem nahrávat vlastní prezentace s tématem spojené. Neváhej a prezentuj!</p>
						<p><a id="nopic" href="priroda/form.php" class="link-style">Prezentace</a></p>
				</div>
				<div class="child">
					<h2> <img src="images/icon.png" alt="Icon"> Technologie</h2>
						<p><img src="images/technologie.jpeg" width="430" height="200" alt="" /></p>
						<p>Máš mít prezentaci a nevíš si rady? Zde nalezneš různé prezentace na téma TECHNOLOGIE.
							Každou z nich si můžeš stáhnout a použít dle libosti. Můžeš zde hledat i pouhou inspiraci nebo sem nahrávat vlastní prezentace s tématem spojené. Neváhej a prezentuj!</p>
						<p><a id="nopic" href="technologie/form.php" class="link-style">Prezentace</a></p>
				</div>
				<div class="child">
					<h2> <img src="images/icon.png" alt="Icon"> Hudba</h2>
						<p><img src="images/music.jpg" width="430" height="200" alt="" /></p>
						<p>Máš mít prezentaci a nevíš si rady? Zde nalezneš různé prezentace na téma HUDBA.
							Každou z nich si můžeš stáhnout a použít dle libosti. Můžeš zde hledat i pouhou inspiraci nebo sem nahrávat vlastní prezentace s tématem spojené. Neváhej a prezentuj!</p>
						<p><a id="nopic" href="hudba/form.php" class="link-style">Prezentace</a></p>
				</div>
			</div>
		</div>
		<div id="footer">
			<p>&copy; Filip Blažek</p>
		</div>
	</div>
</body>
</html>