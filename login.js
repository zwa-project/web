 function validate(udalost) {
     validateEmail(udalost);
     validatePassword(udalost);
 }

 function validateEmail(event) {
     var mail = document.querySelector('#mail');
     var line = document.querySelector('#pmail');
     if (email.value.length == 0) {
         event.preventDefault();
         line.className = 'error';
     } else {
         line.className = '';
     }
       if(mail.value.indexOf('@') == -1) {
           event.preventDefault();
           mail.className = 'error';
       } else {
           mail.className = '';
       }
 }

 function validatePassword(event) {
     var password = document.querySelector('#password');
     var line = document.querySelector('#ppassword');
     if (password.value.length == 0) {
         event.preventDefault();
         line.className = 'error';
     } else {
         line.className = '';
     }
 }

 function init() {
     var formular = document.querySelector('form#login2');
     formular.addEventListener('submit', validate);

     var mail = document.querySelector('#mail');
     var password = document.querySelector('#password');

     mail.addEventListener('blur', validateEmail);
     password.addEventListener('blur', validatePassword)
}